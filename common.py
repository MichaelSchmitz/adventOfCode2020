from typing import List


def read_input(name: str) -> List[str]:
    with open(f'inputs/{name}.txt', 'r') as file:
        return file.readlines()


def print_day_result(day: str, part: int, result: any) -> None:
    print(f'Day {day}, part {part}: {result}')
