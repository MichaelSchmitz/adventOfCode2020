from typing import Tuple, List

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __get_new_range(self, lower: int, upper: int, char: str) -> Tuple[int, int]:
        if char == 'F' or char == 'L':
            return lower, upper - round((upper-lower)/2)
        elif char == 'B' or char == 'R':
            return lower + round((upper-lower)/2), upper

    def __get_row(self, seat_pass_row: str) -> int:
        lower = 0
        upper = 127
        for char in seat_pass_row[:6]:
            lower, upper = self.__get_new_range(lower, upper, char)
        if seat_pass_row[6] == 'F':
            return lower
        else:
            return upper

    def __get_column(self, seat_pass_column: str) -> int:
        left = 0
        right = 7
        for char in seat_pass_column[:2]:
            left, right = self.__get_new_range(left, right, char)
        if seat_pass_column[2] == 'L':
            return left
        else:
            return right

    def __get_seat_id(self, seat_pass: str) -> int:
        row = self.__get_row(seat_pass[:7])
        column = self.__get_column(seat_pass[-3:])
        return row * 8 + column

    def __get_all_seat_ids(self) -> List[int]:
        seat_ids = []
        for seat_pass in self.puzzle_input:
            seat_ids.append(self.__get_seat_id(seat_pass[:10]))
        return seat_ids

    def process_input_part1(self) -> int:
        seat_ids = self.__get_all_seat_ids()
        return max(seat_ids)

    def process_input_part2(self) -> int:
        seat_ids = sorted(self.__get_all_seat_ids())
        diff = set(range(seat_ids[0], seat_ids[len(seat_ids)-1])).difference(seat_ids)
        return diff.pop()
