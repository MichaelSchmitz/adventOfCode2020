from typing import List, Set, Dict

from .AbstractDay import AbstractDay


class Rule:
    def __init__(self, rule: str) -> None:
        self.name, value = rule.split(': ')
        range_one, range_two = value.split(' or ')
        range_one_start, range_one_end = range_one.split('-')
        range_two_start, range_two_end = range_two.split('-')
        self.range_one = range(int(range_one_start), int(range_one_end) + 1)
        self.range_two = range(int(range_two_start), int(range_two_end) + 1)

    def is_valid(self, num: int) -> bool:
        return num in self.range_one or num in self.range_two


class Ticket:
    def __init__(self, ticket: str) -> None:
        self.values = []
        values = ticket.split(',')
        for value in values:
            self.values.append(int(value))

    def get_invalid_field_sum(self, rules: List[Rule]) -> int:
        invalid_sum = 0
        for value in self.values:
            any_valid = False
            for rule in rules:
                if rule.is_valid(value):
                    any_valid = True
            invalid_sum = invalid_sum + value if not any_valid else invalid_sum
        return invalid_sum

    def is_valid_rule_for(self, rule: Rule) -> Set[int]:
        valid = set()
        for i, value in enumerate(self.values):
            if rule.is_valid(value):
                valid.add(i)
        return valid

class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]) -> None:
        super().__init__(puzzle_input)
        processor = Rule
        self.rules = []
        self.tickets = []
        current_list = self.rules
        for line in self.puzzle_input:
            if line == '\n':
                processor = Ticket
                current_list = self.tickets
            else:
                if line not in ['your ticket:\n', 'nearby tickets:\n']:
                    current_list.append(processor(line))

    def get_all_valid_positions(self, tickets: List[Ticket]) -> Dict[str, Set[int]]:
        valid_positions: Dict[str, Set[int]] = {}
        for ticket in tickets:
            for rule in self.rules:
                positions = ticket.is_valid_rule_for(rule)
                # first time
                if rule.name not in valid_positions:
                    valid_positions[rule.name] = positions
                else:
                    valid_positions[rule.name] = valid_positions[rule.name].intersection(positions)
        return valid_positions

    def get_single_valid_positions(self, valid_positions: Dict[str, Set[int]]) -> Dict[str, int]:
        reduced_positions = {}
        while len(valid_positions) > 1:
            for rule, value in valid_positions.items():
                if len(value) == 1:
                    del valid_positions[rule]
                    self.__remove_value_from_sets(valid_positions, value)
                    reduced_positions[rule] = value.pop()
                    break
        return reduced_positions

    def __remove_value_from_sets(self, valid_positions: Dict[str, Set[int]], value: Set[int]):
        for key, values in valid_positions.items():
            valid_positions[key] = values - value

    def process_input_part1(self) -> int:
        invalid_sum = 0
        for ticket in self.tickets:
            invalid_sum = invalid_sum + ticket.get_invalid_field_sum(self.rules)
        return invalid_sum

    def process_input_part2(self) -> int:
        reduced_tickets = []
        for ticket in self.tickets:
            if ticket.get_invalid_field_sum(self.rules) == 0:
                reduced_tickets.append(ticket)

        valid_positions = self.get_all_valid_positions(reduced_tickets)

        single_valid_positions = self.get_single_valid_positions(valid_positions)

        departure_fields = 1
        for key in single_valid_positions.keys():
            if key.startswith('departure'):
                departure_fields = departure_fields * self.tickets[0].values[single_valid_positions[key]]
        return departure_fields
