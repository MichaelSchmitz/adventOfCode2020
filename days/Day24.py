from __future__ import annotations
from typing import Dict, List

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.__starting_positions = []
        for tile in self.puzzle_input:
            position = self.__get_position(tile.strip('\n'))
            if position in self.__starting_positions:
                self.__starting_positions.remove(position)
            else:
                self.__starting_positions.append(position)
        self.__processed_whites = []

    def __get_position(self, tile: str) -> Dict[str, int]:
        position = {'x': 0, 'y': 0}
        while tile != '':
            if tile.startswith('e'):
                tile = tile[1:]
                position['x'] += 2
            elif tile.startswith('se'):
                tile = tile[2:]
                position['x'] += 1
                position['y'] += 1
            elif tile.startswith('sw'):
                tile = tile[2:]
                position['x'] -= 1
                position['y'] += 1
            elif tile.startswith('w'):
                tile = tile[1:]
                position['x'] -= 2
            elif tile.startswith('nw'):
                tile = tile[2:]
                position['x'] -= 1
                position['y'] -= 1
            elif tile.startswith('ne'):
                tile = tile[2:]
                position['x'] += 1
                position['y'] -= 1
            else:
                raise NotImplementedError
        return position

    def __process_day(self, positions: List[Dict[str, int]]):
        __position_tmp = positions.copy()
        self.__processed_whites.clear()
        for position in positions:
            black = 0
            for neighbor in self.__get_neighbors(position):
                if neighbor in positions:
                    black += 1
                else:
                    if neighbor not in self.__processed_whites and self.__should_flip_white(neighbor, positions):
                        __position_tmp.append(neighbor)
                    self.__processed_whites.append(neighbor)
            if black == 0 or black > 2:
                __position_tmp.remove(position)
        return __position_tmp

    def __get_neighbors(self, position: Dict[str, int]) -> List[Dict[str, int]]:
        return [
            {'x': position['x'] + 2, 'y': position['y']},
            {'x': position['x'] + 1, 'y': position['y'] + 1},
            {'x': position['x'] - 1, 'y': position['y'] + 1},
            {'x': position['x'] - 2, 'y': position['y']},
            {'x': position['x'] - 1, 'y': position['y'] - 1},
            {'x': position['x'] + 1, 'y': position['y'] - 1}
        ]

    def __should_flip_white(self, tile: Dict[str, int], blacks: List[Dict[str, int]]) -> bool:
        black = 0
        for neighbor in self.__get_neighbors(tile):
            if neighbor in blacks:
                black += 1
        return black == 2

    def process_input_part1(self) -> int:
        return len(self.__starting_positions)

    def process_input_part2(self) -> int:
        """
        Extremely naive approach: took ~12min to complete
        """
        positions = self.__starting_positions.copy()
        for _ in range(100):
            positions = self.__process_day(positions)
        return len(positions)
