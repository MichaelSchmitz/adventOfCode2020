import re
from typing import List

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __apply_bitmask(self, mask: str, num: int) -> int:
        bits = list(f'{num:036b}')
        for i, char in enumerate(mask):
            if char == '1':
                bits[i] = '1'
            elif char == '0':
                bits[i] = '0'
        return int(''.join(bits), 2)

    def __apply_bitmask_address(self, mask: str, num: int) -> str:
        bits = list(f'{num:036b}')
        for i, char in enumerate(mask):
            if char == '1':
                bits[i] = '1'
            elif char == 'X':
                bits[i] = 'X'
        return ''.join(bits)

    def __get_floating_addresses(self, floating_index: str) -> List[int]:
        addresses: List[int] = []
        if 'X' in floating_index:
            index = floating_index.index('X')
            bits = list(floating_index)
            bits[index] = '0'
            addresses.extend(self.__get_floating_addresses(''.join(bits)))
            bits[index] = '1'
            addresses.extend(self.__get_floating_addresses(''.join(bits)))
            return addresses
        else:
            return [int(floating_index, 2)]


    def process_input_part1(self) -> int:
        mem = {}
        mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        for line in self.puzzle_input:
            if line.startswith('mask'):
                mask = line.lstrip('mask =')
            else:
                groups = re.findall(r'[0-9]+', line)
                index = int(groups[0])
                num = int(groups[1])
                mem[index] = self.__apply_bitmask(mask, num)
        return sum(mem.values())

    def process_input_part2(self) -> int:
        mem = {}
        mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        for line in self.puzzle_input:
            if line.startswith('mask'):
                mask = line.lstrip('mask =')
            else:
                groups = re.findall(r'[0-9]+', line)
                index = int(groups[0])
                num = int(groups[1])
                index = self.__apply_bitmask_address(mask, index)
                for i in self.__get_floating_addresses(index):
                    mem[i] = num
        return sum(mem.values())
