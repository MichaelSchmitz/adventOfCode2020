from typing import List

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.__divider = 20201227
        self.__base = 7
        self.__loop_size_card = -1
        self.__loop_size_door = -1

    def __find_loop_size(self, pub_key: int) -> int:
        loop_size = 0
        val = 1
        while val != pub_key:
            val = (val * self.__base) % self.__divider
            loop_size += 1
        return loop_size

    def __calc_encryption_key(self, loop_size: int, pub_key: int):
        val = 1
        for _ in range(loop_size):
            val = (val * pub_key) % self.__divider
        return val

    def process_input_part1(self) -> int:
        pub_key_card, pub_key_door = int(self.puzzle_input[0]), int(self.puzzle_input[1])
        self.__loop_size_card = self.__find_loop_size(pub_key_card)
        self.__loop_size_door = self.__find_loop_size(pub_key_door)
        encryption_key_card = self.__calc_encryption_key(self.__loop_size_card, pub_key_door)
        encryption_key_door = self.__calc_encryption_key(self.__loop_size_door, pub_key_card)
        if encryption_key_card == encryption_key_door:
            return encryption_key_card
        else:
            raise ValueError

    def process_input_part2(self) -> int:
        pass
