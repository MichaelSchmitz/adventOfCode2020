from typing import Dict

from .AbstractDay import AbstractDay


class Number:
    def __init__(self, turn_count):
        self.turn_count = turn_count
        self.last_turn_count = -1

    def was_spoken_more_than_once(self) -> bool:
        return self.last_turn_count != -1

    def speak(self, turn_count) -> None:
        self.last_turn_count = self.turn_count
        self.turn_count = turn_count

    def get_last_spoken(self) -> int:
        return self.last_turn_count


class Day(AbstractDay):
    def play_game(self, till_number: int) -> int:
        spoken: Dict[int, Number] = {}
        last_num: int = -1
        turn_count = 0
        for num in self.puzzle_input[0].split(','):
            last_num = int(num)
            spoken[last_num] = Number(turn_count)
            turn_count = turn_count + 1

        for i in range(till_number - turn_count):
            if spoken[last_num].was_spoken_more_than_once():
                time_diff = turn_count - 1 - spoken[last_num].get_last_spoken()
                if time_diff in spoken:
                    spoken[time_diff].speak(turn_count)
                else:
                    spoken[time_diff] = Number(turn_count)
                last_num = time_diff
            else:
                spoken[0].speak(turn_count)
                last_num = 0
            turn_count = turn_count + 1
        return last_num

    def process_input_part1(self) -> int:
        return self.play_game(2020)

    def process_input_part2(self) -> int:
        return self.play_game(30000000)
