import regex

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __build_regex(self):
        index = 0
        line = self.puzzle_input[index]
        regexp = ' 0 '
        rules = {}
        while line != '\n':
            rule_key, value = line.split(': ')
            value = value.replace('"', '').replace('\n', '')
            rules[rule_key] = f'( {value} )'
            index = index + 1
            line = self.puzzle_input[index]
        while len(regex.findall(r'[0-9]+', regexp)) > 0:
            for key in regex.findall(r'[0-9]+', regexp):
                regexp = regexp.replace(f' {key} ', f' {rules[key]} ')
        self.regexp = regex.compile(''.join(regexp.split()))
        self.image_start = index + 1

    def __build_regex_with_loop(self):
        index = 0
        line = self.puzzle_input[index]
        regexp = ' 0 '
        rules = {}
        while line != '\n':
            rule_key, value = line.split(': ')
            value = value.replace('"', '').replace('\n', '')
            rules[rule_key] = f'( {value} )'
            index = index + 1
            line = self.puzzle_input[index]
        rules['8'] = ' (?P<groupA> 42 | 42 (?&groupA) ) '
        rules['11'] = ' (?P<groupB> 42 31 | 42 (?&groupB) 31 ) '
        old_regex = ''
        while len(regex.findall(r'[0-9]+', regexp)) > 0 and old_regex != regexp:
            old_regex = regexp
            for key in regex.findall(r'[0-9]+', regexp):
                regexp = regexp.replace(f' {key} ', f' {rules[key]} ')
        self.regexp = regex.compile(''.join(regexp.split()))
        self.image_start = index + 1

    def process_input_part1(self) -> int:
        self.__build_regex()
        count = 0
        for image in self.puzzle_input[self.image_start:]:
            count = count + 1 if self.regexp.fullmatch(image.replace('\n', '')) else count
        return count

    def process_input_part2(self) -> int:
        self.__build_regex_with_loop()
        count = 0
        for image in self.puzzle_input[self.image_start:]:
            count = count + 1 if self.regexp.fullmatch(image.replace('\n', '')) else count
        return count
