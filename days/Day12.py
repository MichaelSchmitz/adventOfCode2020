from typing import List

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.x = 0
        self.y = 0
        self.ship_x = 0
        self.ship_y = 0
        self.degree = 90

    @staticmethod
    def degree_to_dir(degree: int) -> str:
        if degree == 0:
            return 'N'
        elif degree == 90:
            return 'E'
        elif degree == 180:
            return 'S'
        elif degree == 270:
            return 'W'

    def __process_instruction(self, instruction: str) -> None:
        inst = instruction[0]
        value = int(instruction[1:])
        if inst == 'L':
            self.degree = (self.degree - value) % 360
        elif inst == 'R':
            self.degree = (self.degree + value) % 360
        elif inst == 'F':
            self.__process_direction(Day.degree_to_dir(self.degree), value)
        else:
            self.__process_direction(inst, value)

    def __process_instruction_part_two(self, instruction: str) -> None:
        inst = instruction[0]
        value = int(instruction[1:])
        if inst == 'L':
            for i in range(0, value, 90):
                self.__turn_left()
        elif inst == 'R':
            for i in range(0, value, 90):
                self.__turn_right()
        elif inst == 'F':
            self.__move_ship(value)
        else:
            self.__process_direction(inst, value)

    def __move_ship(self, value: int) -> None:
        self.ship_y = self.ship_y + (self.y * value)
        self.ship_x = self.ship_x + (self.x * value)

    def __turn_left(self):
        x = self.x
        self.x = -self.y
        self.y = x

    def __turn_right(self):
        x = self.x
        self.x = self.y
        self.y = -x

    def __process_direction(self, direction: str, value: int) -> None:
        if direction == 'N':
            self.y = self.y + value
        elif direction == 'S':
            self.y = self.y - value
        elif direction == 'E':
            self.x = self.x + value
        elif direction == 'W':
            self.x = self.x - value

    def process_input_part1(self) -> int:
        self.x = 0
        self.y = 0
        self.degree = 90
        for instruction in self.puzzle_input:
            self.__process_instruction(instruction)
        return abs(self.x) + abs(self.y)

    def process_input_part2(self) -> int:
        self.x = 10
        self.y = 1
        for instruction in self.puzzle_input:
            self.__process_instruction_part_two(instruction)
        return abs(self.ship_x) + abs(self.ship_y)
