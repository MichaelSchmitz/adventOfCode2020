from typing import Iterable, Tuple, List, Dict

from .AbstractDay import AbstractDay


class PocketDimension:
    def __init__(self, puzzle_input: List[str]):
        self.__load_dim(puzzle_input)
        self.__load_4dim(puzzle_input)

    def __load_dim(self, puzzle_input: List[str]) -> None:
        self.state = {}
        x, y, z = 0, 0, 0
        for row in puzzle_input:
            for char in row:
                if char != '\n':
                    self.state[(x, y, z)] = char == '#'
                    x = x + 1
            y = y + 1
            x = 0
        for key in list(self.state.keys()):
            for neighbour in self.__get_neighbours(key):
                if neighbour not in self.state:
                    self.state[neighbour] = False

    def __load_4dim(self, puzzle_input: List[str]) -> None:
        self.state_4dim = {}
        x, y, z, w = 0, 0, 0, 0
        for row in puzzle_input:
            for char in row:
                if char != '\n':
                    self.state_4dim[(x, y, z, w)] = char == '#'
                    x = x + 1
            y = y + 1
            x = 0
        for key in list(self.state_4dim.keys()):
            for neighbour in self.__get_neighbours_4dim(key):
                if neighbour not in self.state_4dim:
                    self.state_4dim[neighbour] = False

    def process_iteration(self) -> None:
        temp_state = self.state.copy()
        for key, val in self.state.items():
            if val and self.__check_active(temp_state, key, 2, 3):
                temp_state[key] = True
            elif not val and self.__check_active(temp_state, key, 3, 3):
                temp_state[key] = True
            else:
                temp_state[key] = False
        self.state = temp_state

    def process_iteration_4dim(self) -> None:
        temp_state = self.state_4dim.copy()
        for key, val in self.state_4dim.items():
            if val and self.__check_active_4dim(temp_state, key, 2, 3):
                temp_state[key] = True
            elif not val and self.__check_active_4dim(temp_state, key, 3, 3):
                temp_state[key] = True
            else:
                temp_state[key] = False
        self.state_4dim = temp_state

    def __check_active(self, temp_state: Dict[Tuple[int, int, int], bool], key: Tuple[int, int, int], minimum: int,
                       maximum: int) -> bool:
        count = 0
        for neighbour in self.__get_neighbours(key):
            if neighbour in self.state:
                if count > maximum:
                    return False
                elif self.state[neighbour]:
                    count = count + 1
            else:
                temp_state[neighbour] = False
        return minimum <= count <= maximum

    def __check_active_4dim(self, temp_state: Dict[Tuple[int, int, int, int], bool], key: Tuple[int, int, int, int],
                            minimum: int, maximum: int) -> bool:
        count = 0
        for neighbour in self.__get_neighbours_4dim(key):
            if neighbour in self.state_4dim:
                if count > maximum:
                    return False
                elif self.state_4dim[neighbour]:
                    count = count + 1
            else:
                temp_state[neighbour] = False
        return minimum <= count <= maximum

    def __get_neighbours(self, key: Tuple[int, int, int]) -> Iterable[Tuple[int, int, int]]:
        x, y, z = key
        for x_mod in range(-1, 2):
            for y_mod in range(-1, 2):
                for z_mod in range(-1, 2):
                    if not (x_mod == 0 and y_mod == 0 and z_mod == 0):
                        yield x + x_mod, y + y_mod, z + z_mod

    def __get_neighbours_4dim(self, key: Tuple[int, int, int, int]) -> Iterable[Tuple[int, int, int, int]]:
        x, y, z, w = key
        for x_mod in range(-1, 2):
            for y_mod in range(-1, 2):
                for z_mod in range(-1, 2):
                    for w_mod in range(-1, 2):
                        if not (x_mod == 0 and y_mod == 0 and z_mod == 0 and w_mod == 0):
                            yield x + x_mod, y + y_mod, z + z_mod, w + w_mod


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.pocket_dim = PocketDimension(self.puzzle_input)

    def process_input_part1(self) -> int:
        for i in range(6):
            self.pocket_dim.process_iteration()
        count = 0
        for val in self.pocket_dim.state.values():
            count = count + 1 if val else count
        return count

    def process_input_part2(self) -> int:
        for i in range(6):
            self.pocket_dim.process_iteration_4dim()
        count = 0
        for val in self.pocket_dim.state_4dim.values():
            count = count + 1 if val else count
        return count
