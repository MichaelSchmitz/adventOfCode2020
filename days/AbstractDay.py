from typing import List
from abc import ABC, abstractmethod


class AbstractDay(ABC):
    def __init__(self, puzzle_input: List[str]):
        self.puzzle_input = puzzle_input

    @abstractmethod
    def process_input_part1(self) -> int:
        pass

    @abstractmethod
    def process_input_part2(self) -> int:
        pass
