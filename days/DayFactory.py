from typing import List
from days import *


def get_day(day: str, puzzle_input: List[str]) -> AbstractDay:
    days = {'01': Day01,
            '02': Day02,
            '03': Day03,
            '04': Day04,
            '05': Day05,
            '06': Day06,
            '07': Day07,
            '08': Day08,
            '09': Day09,
            '10': Day10,
            '11': Day11,
            '12': Day12,
            '13': Day13,
            '14': Day14,
            '15': Day15,
            '16': Day16,
            '17': Day17,
            '18': Day18,
            '19': Day19,
            '20': Day20,
            '21': Day21,
            '22': Day22,
            '23': Day23,
            '24': Day24,
            '25': Day25,
            }

    return days.get(day).Day(puzzle_input)

