from typing import List, Tuple, Set, Dict

from .AbstractDay import AbstractDay


class Food:
    def __init__(self, food_desc: str) -> None:
        ingredients, allergens = food_desc.split('(', 1)
        allergens = allergens.rstrip(')\n')
        allergens = allergens.split(' ')[1:]
        self.allergens = []
        for allergen in allergens:
            self.allergens.append(allergen.rstrip(','))
        self.ingredients = list(filter(None, ingredients.split(' ')))


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.foods = []
        for food in self.puzzle_input:
            self.foods.append(Food(food))
        self.ingredients, self.allergens = self.__collect_ingredients_and_allergens()

    def __collect_ingredients_and_allergens(self) -> Tuple[Set[str], Set[str]]:
        ingredients = set()
        allergens = set()
        for food in self.foods:
            ingredients.update(food.ingredients)
            allergens.update(food.allergens)
        return ingredients, allergens

    def __get_non_allergen_ingredients(self) -> Set[str]:
        no_allergen_ingredients = self.ingredients.copy()
        for allergen in self.allergens:
            possible_ingredients = None
            for food in self.foods:
                if allergen in food.allergens:
                    if possible_ingredients is None:
                        possible_ingredients = set(food.ingredients)
                    else:
                        possible_ingredients = possible_ingredients.intersection(food.ingredients)
            if len(possible_ingredients) == 1:
                ingredient = possible_ingredients.pop()
                no_allergen_ingredients.discard(ingredient)
            else:
                no_allergen_ingredients = no_allergen_ingredients - possible_ingredients
        return no_allergen_ingredients

    def __create_allergen_map(self) -> Dict[str, Set[str]]:
        allergens = {}
        # map all allergens which have only one possible ingredient
        for allergen in self.allergens:
            possible_ingredients = None
            for food in self.foods:
                if allergen in food.allergens:
                    if possible_ingredients is None:
                        possible_ingredients = set(food.ingredients)
                    else:
                        possible_ingredients = possible_ingredients.intersection(food.ingredients)

            if len(possible_ingredients) == 1:
                ingredient = possible_ingredients.pop()
                allergens[allergen] = ingredient

            else:
                allergens[allergen] = possible_ingredients
        # map allergens with multiple possible ingredients
        allergens_with_set = dict(filter(lambda item: isinstance(item[1], set), allergens.items()))
        while len(allergens_with_set) > 0:
            for allergen, possible_ingredients in allergens_with_set.items():
                for ingredient in allergens.values():
                    if isinstance(ingredient, str):
                        possible_ingredients.discard(ingredient)

                if len(possible_ingredients) == 1:
                    possible_ingredients = possible_ingredients.pop()
                    allergens[allergen] = possible_ingredients
                    del allergens_with_set[allergen]
                    break
        return allergens

    def process_input_part1(self) -> int:
        no_allergen_ingredients = self.__get_non_allergen_ingredients()
        count = 0
        for found_ingredient in no_allergen_ingredients:
            for food in self.foods:
                count = count + 1 if found_ingredient in food.ingredients else count
        return count

    def process_input_part2(self) -> int:
        allergens = self.__create_allergen_map()
        print(','.join(dict(sorted(allergens.items())).values()))
        return -1
