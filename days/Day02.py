from typing import Tuple
from .AbstractDay import AbstractDay


class Policy:
    def __init__(self, policy: str):
        split = policy.split(' ')
        split_bounds = split[0].split('-')
        self.lower_bound = int(split_bounds[0])
        self.upper_bound = int(split_bounds[1])
        self.char = split[1]

    def is_valid_policy_1(self, password) -> bool:
        return password.count(self.char) in range(self.lower_bound, self.upper_bound + 1)

    def is_valid_policy_2(self, password) -> bool:
        return (password[self.lower_bound - 1] == self.char) ^ (password[self.upper_bound - 1] == self.char)


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        count = 0
        for password_policy in self.puzzle_input:
            password_policy = Day.__split_for_policy(password_policy)
            count = count + 1 if password_policy[0].is_valid_policy_1(password_policy[1]) else count
        return count

    def process_input_part2(self) -> int:
        count = 0
        for password_policy in self.puzzle_input:
            password_policy = Day.__split_for_policy(password_policy)
            count = count + 1 if password_policy[0].is_valid_policy_2(password_policy[1]) else count
        return count

    @staticmethod
    def __split_for_policy(password_policy: str) -> Tuple[Policy, str]:
        split = password_policy.split(': ')
        return Policy(split[0]), split[1]
