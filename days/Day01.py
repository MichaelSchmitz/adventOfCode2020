from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        for num in self.puzzle_input:
            num = int(num)
            for num2 in self.puzzle_input:
                num2 = int(num2)
                if num + num2 == 2020:
                    return num * num2

    def process_input_part2(self) -> int:
        for num in self.puzzle_input:
            num = int(num)
            for num2 in self.puzzle_input:
                num2 = int(num2)
                # takes longer
                # if not __is_greater(num, num2):
                for num3 in self.puzzle_input:
                    num3 = int(num3)
                    if num + num2 + num3 == 2020:
                        return num * num2 * num3


def __is_greater(num1: int, num2: int) -> bool:
    return num1 + num2 > 2020
