from typing import List, Tuple

from .AbstractDay import AbstractDay

class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.instructions = {
            'acc': self.__do_acc,
            'jmp': self.__do_jmp,
            'nop': self.__do_nop
        }

    def __get_instruction_and_arg(self, index: int) -> Tuple[str, int]:
        line = self.puzzle_input[index]
        inst_and_arg = line.split()
        return inst_and_arg[0], int(inst_and_arg[1])

    def __do_acc(self, arg: int) -> None:
        self.accumulator = self.accumulator + arg
        self.index = self.index + 1

    def __do_jmp(self, arg: int) -> None:
        self.index = self.index + arg

    def __do_nop(self, arg: int) -> None:
        self.index = self.index + 1

    def __perform_program(self) -> int:
        self.accumulator = 0
        self.index = 0
        self.visited = []
        while self.index not in self.visited and self.index < len(self.puzzle_input):
            self.visited.append(self.index)
            instruction, arg = self.__get_instruction_and_arg(self.index)
            self.instructions[instruction](arg)
        return self.accumulator

    def process_input_part1(self) -> int:
        return self.__perform_program()

    def process_input_part2(self) -> int:
        # brute force try every replacement
        puzzle_unmodified = self.puzzle_input.copy()
        for i, line in enumerate(self.puzzle_input):
            if line.startswith('nop'):
                line = line.replace('nop', 'jmp')
                self.puzzle_input[i] = line
            elif line.startswith('jmp'):
                line = line.replace('jmp', 'nop')
                self.puzzle_input[i] = line
            else:
                continue
            result = self.__perform_program()
            if self.index == len(self.puzzle_input):
                return result
            self.puzzle_input = puzzle_unmodified.copy()
