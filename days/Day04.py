import re

from .AbstractDay import AbstractDay


class Passport:
    def __init__(self, passport: str):
        self.passport = passport
        self.birth_year = self.__extract_param('byr')
        self.issue_year = self.__extract_param('iyr')
        self.expiration_year = self.__extract_param('eyr')
        self.height = self.__extract_param('hgt')
        self.hair_color = self.__extract_param('hcl')
        self.eye_color = self.__extract_param('ecl')
        self.passport_id = self.__extract_param('pid')
        self.country_id = self.__extract_param('cid')

    def __extract_param(self, param_name: str) -> str:
        if f'{param_name}:' in self.passport:
            return re.findall(rf'{param_name}:([^ \n]*)[ \n]', self.passport)[0]
        else:
            return ''

    def has_all_fields(self) -> bool:
        return '' not in [self.birth_year, self.issue_year, self.expiration_year, self.height,
                          self.hair_color, self.eye_color, self.passport_id]

    def is_valid(self) -> bool:
        if self.has_all_fields():
            return int(self.birth_year) in range(1920, 2003) and\
                   int(self.issue_year) in range(2010, 2021) and\
                   int(self.expiration_year) in range(2020, 2031) and\
                   self.__valid_height() and\
                   bool(re.match(r'\A#[0-9a-f]{6}\Z', self.hair_color)) and\
                   bool(re.match(r'\A(amb|blu|brn|gry|grn|hzl|oth)\Z', self.eye_color)) and\
                   bool(re.match(r'\A[0-9]{9}\Z', self.passport_id))
        else:
            return False

    def __valid_height(self) -> bool:
        if 'cm' in self.height:
            height = self.height.rstrip('cm')
            return int(height) in range(150, 194)
        elif 'in' in self.height:
            height = self.height.rstrip('in')
            return int(height) in range(59, 77)
        else:
            return False


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        puzzle = ''.join(self.puzzle_input).split('\n\n')
        valid_count = 0
        for passport_in in puzzle:
            passport = Passport(f'{passport_in}\n')
            valid_count = valid_count + 1 if passport.has_all_fields() else valid_count
        return valid_count

    def process_input_part2(self) -> int:
        puzzle = ''.join(self.puzzle_input).split('\n\n')
        valid_count = 0
        for passport_in in puzzle:
            passport = Passport(f'{passport_in}\n')
            valid_count = valid_count + 1 if passport.is_valid() else valid_count
        return valid_count
