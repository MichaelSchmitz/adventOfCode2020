from math import lcm
from typing import List, Tuple

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        start = int(self.puzzle_input[0])
        busses = self.puzzle_input[1].split(',')
        earliest = (100000000, 0)
        for bus in busses:
            if bus != 'x':
                bus = int(bus)
                offset = start % bus
                wait = offset if offset == 0 else -offset + bus
                if wait < earliest[0]:
                    earliest = (wait, bus)
        return earliest[0] * earliest[1]

    def process_input_part2(self) -> int:
        busses = self.puzzle_input[1].split(',')
        busses_with_offset: List[Tuple[int, int]] = [(1, 0)]
        timestamp = 0
        # optimisation from exercise, but not really necessary (0.1ms diff)
        # timestamp = 100000000000000
        for i, bus in enumerate(busses):
            if bus != 'x':
                busses_with_offset.append((int(bus), i))
        least_common_multiple = [1]
        timestamp = timestamp + busses_with_offset[1][0] - timestamp % busses_with_offset[1][0]
        for i in range(len(busses_with_offset) - 1):
            timestamp, least_common_multiple = self.__perform_for_pair(timestamp, least_common_multiple, busses_with_offset[i], busses_with_offset[i + 1])
        return timestamp

    def __perform_for_pair(self, timestamp: int, last_lcm: List[int], bus1: Tuple[int, int], bus2: Tuple[int, int]) -> Tuple[int, List[int]]:
        least_common_multiple = lcm(*last_lcm)
        while not self.__check_condition(timestamp, bus1, bus2):
            timestamp = timestamp + least_common_multiple
        least_common_multiple = lcm(bus2[0], *last_lcm)
        last_lcm.append(least_common_multiple)
        return timestamp, last_lcm

    def __check_condition(self, timestamp: int, bus0: Tuple[int, int], bus1: Tuple[int, int]) -> bool:
        return (timestamp + bus0[1]) % bus0[0] == 0 and (timestamp + bus1[1]) % bus1[0] == 0
