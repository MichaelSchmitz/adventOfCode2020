from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __process_slope(self, right_factor: int, down_factor: int) -> int:
        x_pos = 0
        trees = 0
        # -1 cause of line break
        mod_factor = len(self.puzzle_input[0]) - 1
        for row_index in range(0, len(self.puzzle_input), down_factor):
            row = self.puzzle_input[row_index]
            trees = trees + 1 if row[x_pos % mod_factor] == '#' else trees
            x_pos = x_pos + right_factor
        return trees

    def process_input_part1(self) -> int:
        return self.__process_slope(3, 1)

    def process_input_part2(self) -> int:
        return self.__process_slope(1, 1) *\
               self.__process_slope(3, 1) *\
               self.__process_slope(5, 1) *\
               self.__process_slope(7, 1) *\
               self.__process_slope(1, 2)
