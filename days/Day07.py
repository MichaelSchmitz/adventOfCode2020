from typing import Dict, List, Set, Tuple
import re
from .AbstractDay import AbstractDay


class Bag:
    all_bags: Dict = {}

    def __init__(self, bag: str):
        self.inner_bags = []
        self.name = bag
        Bag.all_bags[bag] = self

    def add_inner_bags(self, inner_bags: List[Tuple[str, str]]) -> None:
        for inner_bag in inner_bags:
            if inner_bag[1] != 'no other':
                self.inner_bags.append((int(inner_bag[0]),
                                        Bag.get_bag_and_create_if_not_existent(re.sub('[0-9]*', '', inner_bag[1]))))

    def contains_inner_bag(self, name: str) -> bool:
        for bag in self.inner_bags:
            if bag[1].name == name:
                return True
        return False

    def count_inner_bags(self) -> int:
        count = 1
        for bag in self.inner_bags:
            count = count + bag[1].count_inner_bags() * bag[0]
        return count

    @staticmethod
    def get_bag_and_create_if_not_existent(bag_name: str):
        bag_name = ''.join(bag_name.split())
        if bag_name not in Bag.all_bags:
            Bag(bag_name)
        return Bag.all_bags[bag_name]

    @staticmethod
    def get_wrapper_for_bag(name: str) -> List[str]:
        wrappers = []
        for i, bag in enumerate(Bag.all_bags.values()):
            wrappers.append(bag.name) if bag.contains_inner_bag(name) else True
        return wrappers

    @staticmethod
    def find_wrapper_bags(name: str) -> Set[str]:
        wrapper = Bag.get_wrapper_for_bag(name)
        wrapper_bags = {name}
        for bag in wrapper:
            wrapper_bags = wrapper_bags.union(Bag.find_wrapper_bags(bag))
        return wrapper_bags


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        for bag_str in self.puzzle_input:
            outer_bag = re.findall(r'^\w+ \w+', bag_str)[0]
            inner_bags = re.findall(r'(\d) (\w+ \w+)', bag_str)
            if outer_bag != 'no other':
                bag = Bag.get_bag_and_create_if_not_existent(outer_bag)
                bag.add_inner_bags(inner_bags)

    def process_input_part1(self) -> int:
        return len(Bag.find_wrapper_bags('shinygold')) - 1

    def process_input_part2(self) -> int:
        return Bag.all_bags['shinygold'].count_inner_bags() - 1
