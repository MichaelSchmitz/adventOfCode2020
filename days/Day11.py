from __future__ import annotations

from abc import abstractmethod
from enum import Enum
from typing import List, Tuple

from .AbstractDay import AbstractDay


class Seat:
    def __init__(self, initial_seat_state: str, x_pos: int, y_pos: int):
        self.state = SeatState.get_by_value(initial_seat_state)
        if self.state == SeatState.FLOOR:
            self.old_state = self.state
        else:
            self.old_state = None
        self.x_pos = x_pos
        self.y_pos = y_pos

    def process_iteration(self) -> None:
        if self.state is SeatState.FLOOR:
            return
        elif self.state is SeatState.EMPTY and self.adjacent_empty():
            self.state = SeatState.OCCUPIED
        elif self.state is SeatState.OCCUPIED and self.adjacent_more_than_occupied():
            self.state = SeatState.EMPTY

    def changed(self) -> bool:
        return self.state != self.old_state

    @classmethod
    def add_to_list(cls, inst: Seat) -> None:
        if len(cls.get_seats()) <= inst.y_pos:
            cls.get_seats().append([])
        cls.get_seats()[inst.y_pos].append(inst)

    @abstractmethod
    def adjacent_more_than_occupied(self) -> bool:
        pass

    @abstractmethod
    def adjacent_empty(self) -> bool:
        pass

    @classmethod
    @abstractmethod
    def get_seats(cls) -> List[List[Seat]]:
        pass

    @classmethod
    def was_occupied(cls, y_pos: int, x_pos: int) -> bool:
        if len(cls.get_seats()) > y_pos >= 0 and len(cls.get_seats()[y_pos]) > x_pos >= 0:
            return cls.get_seats()[y_pos][x_pos].old_state is SeatState.OCCUPIED
        else:
            return False

    @classmethod
    def difference_in_lists(cls) -> bool:
        for row in cls.get_seats():
            for seat in row:
                if seat.changed():
                    return True
        return False

    @classmethod
    def get_occupied_count(cls) -> int:
        count = 0
        for row in cls.get_seats():
            for seat in row:
                if seat.state is SeatState.OCCUPIED:
                    count = count + 1
        return count

    @classmethod
    def apply_rules_till_stable_state(cls) -> None:
        while cls.difference_in_lists():
            for row in cls.get_seats():
                for seat in row:
                    seat.old_state = seat.state
            for row in cls.get_seats():
                # echo = ''
                for seat in row:
                    seat.process_iteration()
                    # echo = echo + seat.state.value
                # print(echo)
            # print()


class SeatPartOne(Seat):
    __seats: List[List[Seat]] = []

    def adjacent_empty(self) -> bool:
        return not SeatPartOne.was_occupied(self.y_pos - 1, self.x_pos - 1) and \
           not SeatPartOne.was_occupied(self.y_pos - 1, self.x_pos) and \
           not SeatPartOne.was_occupied(self.y_pos - 1, self.x_pos + 1) and \
           not SeatPartOne.was_occupied(self.y_pos, self.x_pos - 1) and \
           not SeatPartOne.was_occupied(self.y_pos, self.x_pos + 1) and \
           not SeatPartOne.was_occupied(self.y_pos + 1, self.x_pos - 1) and \
           not SeatPartOne.was_occupied(self.y_pos + 1, self.x_pos) and \
           not SeatPartOne.was_occupied(self.y_pos + 1, self.x_pos + 1)

    def adjacent_more_than_occupied(self) -> bool:
        count = 0
        count = count + 1 if SeatPartOne.was_occupied(self.y_pos - 1, self.x_pos - 1) else count
        count = count + 1 if SeatPartOne.was_occupied(self.y_pos - 1, self.x_pos) else count
        count = count + 1 if SeatPartOne.was_occupied(self.y_pos - 1, self.x_pos + 1) else count
        count = count + 1 if SeatPartOne.was_occupied(self.y_pos, self.x_pos - 1) else count
        count = count + 1 if SeatPartOne.was_occupied(self.y_pos, self.x_pos + 1) else count
        count = count + 1 if SeatPartOne.was_occupied(self.y_pos + 1, self.x_pos - 1) else count
        count = count + 1 if SeatPartOne.was_occupied(self.y_pos + 1, self.x_pos) else count
        count = count + 1 if SeatPartOne.was_occupied(self.y_pos + 1, self.x_pos + 1) else count
        return count > 3

    @classmethod
    def get_seats(cls) -> List[List[Seat]]:
        return SeatPartOne.__seats


class SeatPartTwo(Seat):
    __seats: List[List[Seat]] = []

    def was_occupied_in_dir(self, direction: Tuple[int, int]) -> bool:
        y_pos = self.y_pos + direction[0]
        x_pos = self.x_pos + direction[1]
        while SeatPartTwo.is_floor(y_pos, x_pos):
            y_pos = y_pos + direction[0]
            x_pos = x_pos + direction[1]
        return SeatPartTwo.was_occupied(y_pos, x_pos)

    def adjacent_empty(self) -> bool:
        return not self.was_occupied_in_dir((-1, -1)) and \
               not self.was_occupied_in_dir((-1, 0)) and \
               not self.was_occupied_in_dir((-1, 1)) and \
               not self.was_occupied_in_dir((0, -1)) and \
               not self.was_occupied_in_dir((0, +1)) and \
               not self.was_occupied_in_dir((1, -1)) and \
               not self.was_occupied_in_dir((1, 0)) and \
               not self.was_occupied_in_dir((1, 1))

    def adjacent_more_than_occupied(self) -> bool:
        count = 0
        count = count + 1 if self.was_occupied_in_dir((-1, -1)) else count
        count = count + 1 if self.was_occupied_in_dir((-1, 0)) else count
        count = count + 1 if self.was_occupied_in_dir((-1, 1)) else count
        count = count + 1 if self.was_occupied_in_dir((0, -1)) else count
        count = count + 1 if self.was_occupied_in_dir((0, 1)) else count
        count = count + 1 if self.was_occupied_in_dir((1, -1)) else count
        count = count + 1 if self.was_occupied_in_dir((1, 0)) else count
        count = count + 1 if self.was_occupied_in_dir((1, 1)) else count
        return count > 4

    @classmethod
    def get_seats(cls) -> List[List[Seat]]:
        return cls.__seats

    @classmethod
    def is_floor(cls, y_pos: int, x_pos: int) -> bool:
        if len(cls.get_seats()) > y_pos >= 0 and len(cls.get_seats()[y_pos]) > x_pos >= 0:
            return cls.get_seats()[y_pos][x_pos].state is SeatState.FLOOR
        else:
            return False


class SeatState(Enum):
    FLOOR = '.'
    EMPTY = 'L'
    OCCUPIED = '#'

    @staticmethod
    def get_by_value(val: str) -> SeatState:
        if val == SeatState.FLOOR.value:
            return SeatState.FLOOR
        if val == SeatState.EMPTY.value:
            return SeatState.EMPTY
        if val == SeatState.OCCUPIED.value:
            return SeatState.OCCUPIED


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        for y, row in enumerate(self.puzzle_input):
            for x, seat in enumerate(row):
                if seat != '\n':
                    SeatPartOne.add_to_list(SeatPartOne(seat, x, y))
                    SeatPartTwo.add_to_list(SeatPartTwo(seat, x, y))

    def process_input_part1(self) -> int:
        SeatPartOne.apply_rules_till_stable_state()
        return SeatPartOne.get_occupied_count()

    def process_input_part2(self) -> int:
        SeatPartTwo.apply_rules_till_stable_state()
        return SeatPartTwo.get_occupied_count()
