from functools import lru_cache
from typing import List, Tuple

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.adapters = []
        for adapter in self.puzzle_input:
            self.adapters.append(int(adapter))
        self.adapters.sort()
        self.adapters.append(max(self.adapters) + 3)

    def __count_jolt_diffs(self) -> Tuple[int, int, int]:
        one_jolt = 0
        two_jolt = 0
        three_jolt = 0
        last_adapter = 0
        for adapter in self.adapters:
            diff = adapter - last_adapter
            if diff == 1:
                one_jolt = one_jolt + 1
            elif diff == 2:
                two_jolt = two_jolt + 1
            elif diff == 3:
                three_jolt = three_jolt + 1
            last_adapter = adapter
        return one_jolt, two_jolt, three_jolt

    @lru_cache(maxsize=None)
    def __get_possibilities_for_adapter(self, adapter: int) -> int:
        possibilities = 0
        count = len(self.adapters) - 1
        if self.adapters[count] == adapter:
            return 1
        else:
            if adapter + 1 in self.adapters:
                possibilities = possibilities + self.__get_possibilities_for_adapter(adapter + 1)
            if adapter + 2 in self.adapters:
                possibilities = possibilities + self.__get_possibilities_for_adapter(adapter + 2)
            if adapter + 3 in self.adapters:
                possibilities = possibilities + self.__get_possibilities_for_adapter(adapter + 3)
        return possibilities

    def process_input_part1(self) -> int:
        one_jolt, _, three_jolt = self.__count_jolt_diffs()
        return one_jolt * three_jolt

    def process_input_part2(self) -> int:
        return self.__get_possibilities_for_adapter(0)
