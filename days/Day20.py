from __future__ import annotations

from typing import List, Dict
from itertools import product
from math import sqrt
import numpy as np

from .AbstractDay import AbstractDay


class Tile:
    def __init__(self, tile: List[str]) -> None:
        _, self.id = tile[0].split(' ')
        self.id = int(self.id[:-2])
        self.data = []
        for line in tile[1:]:
            if line != '\n':
                line = line.rstrip('\n')
                self.data.append([char.replace('.', '0').replace('#', '1') for char in line])

    def get_all_sides(self) -> Dict[str, str]:
        return {'l': self.get_left(),
                'r': self.get_right(),
                't': self.get_top(),
                'b': self.get_bottom()
                }

    def get_all_sides_and_inverted(self) -> Dict[str, str]:
        sides = self.get_all_sides()
        result = sides.copy()
        for key, value in sides.items():
            result[f'i{key}'] = self.get_inverted(value)
        return result

    def get_left(self) -> str:
        res = ''
        for line in self.data:
            res = res + line[0]
        return res

    def get_right(self) -> str:
        res = ''
        for line in self.data:
            res = res + line[-1]
        return res

    def get_top(self) -> str:
        return ''.join(self.data[0])

    def get_bottom(self) -> str:
        return ''.join(self.data[-1])

    def get_inverted(self, data: str) -> str:
        return ''.join(reversed(data))

    def find_matching_count(self, tiles: List[Tile]) -> int:
        matching = set()
        for tile in tiles:
            if tile.id != self.id:
                for combination in product(self.get_all_sides().items(), tile.get_all_sides_and_inverted().items()):
                    if combination[0][1] == combination[1][1]:
                        matching.add(tile.id)
        return len(matching)

    def rot90(self):
        self.data = np.rot90(self.data)

    def flip_h(self):
        self.data = np.fliplr(self.data)

    def flip_v(self):
        self.data = np.flipud(self.data)


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.tiles = []
        self.image = {}
        self.reduced_image = ''
        self.__load_tiles()
        self.width = int(sqrt(len(self.tiles)))
        self.__find_corners()

    def __find_corners(self):
        self.corners = []
        for tile in self.tiles:
            if tile.find_matching_count(self.tiles) == 2:
                self.corners.append(tile.id)

    def __load_tiles(self):
        tile = []
        for line in self.puzzle_input:
            if line.startswith('Tile') and len(tile) > 0:
                self.tiles.append(Tile(tile))
                tile = [line]
            else:
                tile.append(line)
        self.tiles.append(Tile(tile))

    def __find_matching(self, tiles: List[Tile], side: str, required_side: str) -> Tile:
        for tile in tiles:
            matching_sides = []
            for tile_side_key, tile_side in tile.get_all_sides_and_inverted().items():
                if tile_side == side:
                    matching_sides.append(tile_side_key)
                    if required_side == 'l':
                        if tile_side_key.endswith('r'):
                            tile.flip_h()
                        elif tile_side_key.endswith('t'):
                            tile.rot90()
                            tile.flip_v()
                        elif tile_side_key.endswith('b'):
                            tile.rot90()
                            tile.flip_v()
                            tile.flip_h()
                        if tile_side_key.startswith('i'):
                            tile.flip_v()
                    # top
                    else:
                        if tile_side_key.endswith('r'):
                            tile.rot90()
                        elif tile_side_key.endswith('l'):
                            tile.rot90()
                            tile.flip_v()
                        elif tile_side_key.endswith('b'):
                            tile.flip_v()
                        if tile_side_key.startswith('i'):
                            tile.flip_h()
                    return tile

    def __build_image(self):
        starting_corner = list(filter(lambda _tile: _tile.id in self.corners, self.tiles))[0]
        sides = ''
        for tile in self.tiles:
            if tile.id != starting_corner.id:
                for combination in product(starting_corner.get_all_sides().items(), tile.get_all_sides_and_inverted().items()):
                    sides = sides + combination[0][0] if combination[0][1] == combination[1][1] else sides
        if sides in ['rt', 'tr']:
            starting_corner.flip_v()
        elif sides in ['lb', 'bl']:
            starting_corner.rot90()
        elif sides in ['lt', 'tl']:
            starting_corner.flip_h()
            starting_corner.flip_v()
        self.image = {(0, 0): starting_corner}
        unprocessed = self.tiles.copy()
        unprocessed.remove(starting_corner)
        x, y = 1, 0
        next_side, required_side = starting_corner.get_right(), 'l'
        while len(unprocessed) > 0:
            tile = self.__find_matching(unprocessed, next_side, required_side)
            if tile is None:
                raise ValueError(f'No tile found at pos x: {x}, y: {y}')
            unprocessed.remove(tile)
            self.image[(x, y)] = tile
            x = x + 1
            required_side = 'l'
            if self.width == x:
                y = y + 1
                x = 0
                next_side = self.image[(0, y - 1)].get_bottom()
                required_side = 't'
            elif required_side == 'l':
                next_side = tile.get_right()

    def __reduce_image(self) -> np.ndarray:
        reduced = []
        last_y = -1
        for pos, tile in self.image.items():
            x, y = pos
            binary = self.__tile_to_binary(tile)
            if last_y != y:
                last_y = y
                reduced.append(binary)
            else:
                reduced[last_y] = np.concatenate((reduced[last_y], binary), axis=1)
        reduced = np.concatenate(reduced, axis=0)
        # remove borders
        reduced = np.delete(reduced, list(range(0, reduced.shape[0], 10)) + list(range(9, reduced.shape[0], 10)), axis=0)
        reduced = np.delete(reduced, list(range(0, reduced.shape[1], 10)) + list(range(9, reduced.shape[1], 10)), axis=1)

        return reduced

    def __tile_to_binary(self, tile: Tile) -> np.ndarray:
        if isinstance(tile.data, np.ndarray):
            binary = tile.data.astype(np.int)
        else:
            binary = np.array(tile.data).astype(np.int)
        return binary

    def __find_pattern(self, pattern: str):
        pattern = pattern.replace(" ", "0 ").replace("#", "1 ").split("\n")
        pattern = np.array([[int(x) for x in line.strip().split(" ")] for line in pattern])
        monster_length = np.sum(pattern)
        reduced_binary_image = self.__reduce_image()
        patterns = []
        for i in range(4):
            rot_pattern = np.rot90(pattern, i)
            patterns.append(rot_pattern)
            patterns.append(np.fliplr(rot_pattern))
            patterns.append(np.flipud(rot_pattern))

        monsters = 0
        while monsters == 0 and len(patterns) > 0:
            monster = patterns.pop()
            monster_x, monster_y = monster.shape
            for x, y in np.ndindex(reduced_binary_image.shape):
                sub_arr = reduced_binary_image[x: x + monster_x, y: y + monster_y].copy()
                if not sub_arr.shape == monster.shape:
                    continue
                sub_arr = sub_arr * monster

                if np.sum(sub_arr) == monster_length:
                    monsters = monsters + 1
                    reduced_binary_image[x: x + monster_x, y: y + monster_y] = \
                        reduced_binary_image[x: x + monster_x, y: y + monster_y] - monster
        if monsters == 0:
            print('WARNING: No monster found')
        return reduced_binary_image

    def process_input_part1(self) -> int:
        result = 1
        for corner in self.corners:
            result = result * corner
        return result

    def process_input_part2(self) -> int:
        pattern = '                  # \n#    ##    ##    ###\n #  #  #  #  #  #   '
        self.__build_image()
        without_monster = self.__find_pattern(pattern)
        return int(np.sum(without_monster))
