import re

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __solve(self, formula: str, solver) -> int:
        while '(' in formula:
            for match in re.finditer(r'\([0-9+*]*?\)', formula):
                result = self.__solve(match.group(0)[1:-1], solver)
                formula = formula.replace(match.group(0), str(result))
        return solver(formula)

    def __solve_without_precedence(self, formula_part: str) -> int:
        matches = re.split(r'([+*])', formula_part)
        if len(matches) > 2:
            right = int(matches[-1])
            matches.pop()
            operator = matches[-1]
            matches.pop()
            left = ''.join(matches)
            return eval(f'{self.__solve_without_precedence(left)}{operator}{right}')
        else:
            return int(formula_part)

    def __solve_with_inverted_precedence(self, formula: str) -> int:
        while '+' in formula:
            matches = re.split(r'([+*])', formula)
            for i, match in enumerate(matches):
                if match == '+':
                    left = matches[i-1]
                    right = matches[i+1]
                    result = eval(f'{left}+{right}')
                    formula = formula.replace(f'{left}+{right}', str(result), 1)
                    # use new formula with replaced addition
                    break
        return eval(formula)

    def process_input_part1(self) -> int:
        result = 0
        for formula in self.puzzle_input:
            # remove whitespaces/newlines
            formula = ''.join(formula.split(sep=None))
            result = result + self.__solve(formula, self.__solve_without_precedence)
        return result

    def process_input_part2(self) -> int:
        result = 0
        for formula in self.puzzle_input:
            # remove whitespaces/newlines
            formula = ''.join(formula.split(sep=None))
            result = result + self.__solve(formula, self.__solve_with_inverted_precedence)
        return result
