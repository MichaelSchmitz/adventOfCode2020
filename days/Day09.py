import queue
from typing import List

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.invalid_num = -1
        self.queue = queue.Queue(25)

    def __is_sum_of_two(self, num: int) -> bool:
        all_nums = list(self.queue.queue)
        for all_num in all_nums:
            for all_num2 in all_nums:
                if all_num + all_num2 == num:
                    return True
        return False

    def process_input_part1(self) -> int:
        first_done = False
        for num in self.puzzle_input:
            num = int(num)
            if first_done and not self.__is_sum_of_two(num):
                self.invalid_num = num
                return num
            if self.queue.full():
                first_done = True
                self.queue.get()
            self.queue.put(num)

    def process_input_part2(self) -> int:
        if self.invalid_num == -1:
            self.process_input_part1()
        nums = []
        for num in self.puzzle_input:
            nums.append(int(num))
        max_end = nums.index(self.invalid_num) - 1
        for start in range(max_end):
            for end in range(start + 1, max_end):
                num_range = nums[start:end + 1]
                sum_num = sum(num_range)
                if sum_num > self.invalid_num:
                    break
                elif sum_num == self.invalid_num:
                    return min(num_range) + max(num_range)
