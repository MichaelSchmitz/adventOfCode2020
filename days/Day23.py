from typing import List

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.cups = []
        for num in self.puzzle_input[0]:
            self.cups.append(int(num))
        self.size = len(self.cups)
        self.cups_mill = self.__build_map()
        self.size_mill = len(self.cups_mill) - 1
        self.starting_cup = self.cups[0]

    def __build_map(self) -> List[int]:
        cup_map = []
        for i in range(1, 1000002):
            cup_map.append(i)
        for i, v in enumerate(self.cups):
            if self.size > i + 1:
                cup_map[v] = self.cups[i + 1]
            else:
                cup_map[v] = self.size + 1
        cup_map[1000000] = self.cups[0]
        return cup_map

    def lower_cup(self, cup: int, size: int) -> int:
        cup = (cup - 1) % size
        if cup == 0:
            cup = 9
        return cup

    def __print_labels(self) -> None:
        start = self.cups.index(1)
        result = ''
        for i in range(1, self.size):
            result = result + str(self.cups[(start + i) % self.size])
        print(result)

    def __get_stars(self) -> int:
        star1 = self.cups_mill[1]
        star2 = self.cups_mill[star1]
        return star1 * star2

    def __pick_up(self, cup_pos: int) -> List[int]:
        sub_arr = self.cups[cup_pos + 1:cup_pos + 4]
        counter = 0
        while len(sub_arr) < 3:
            sub_arr.append(self.cups[counter])
            counter = counter + 1
        for entry in sub_arr:
            self.cups.remove(entry)
        return sub_arr

    def process_input_part1(self) -> int:
        cup_pos = 0
        for _ in range(100):
            current_cup = self.cups[cup_pos]
            sub_list = self.__pick_up(cup_pos)
            destination_cup = self.lower_cup(current_cup, self.size)
            while destination_cup not in self.cups:
                destination_cup = self.lower_cup(destination_cup, self.size)
            destination_index = self.cups.index(destination_cup)
            for i, cup in enumerate(sub_list):
                self.cups.insert((destination_index + i + 1) % self.size, cup)
            cup_pos = (self.cups.index(current_cup) + 1) % self.size
        self.__print_labels()
        return -1

    def process_input_part2(self) -> int:
        current_cup = self.starting_cup
        for _ in range(10000000):
            # on given position k (a cup) stands the next position v of the cup right next to k
            pick_up1 = self.cups_mill[current_cup]
            pick_up2 = self.cups_mill[pick_up1]
            pick_up3 = self.cups_mill[pick_up2]

            destination_cup = (current_cup - 2) % self.size_mill + 1
            while destination_cup in [pick_up1, pick_up2, pick_up3]:
                destination_cup = (destination_cup - 2) % self.size_mill + 1

            right_of_gap = self.cups_mill[pick_up3]
            old_right_of_destination = self.cups_mill[destination_cup]

            self.cups_mill[current_cup] = right_of_gap           # next cup after the 3 picked up ones
            self.cups_mill[destination_cup] = pick_up1           # destination now points to first picked up cup
            self.cups_mill[pick_up3] = old_right_of_destination  # third picked up cup points two previos right of dest

            current_cup = self.cups_mill[current_cup]
        return self.__get_stars()
