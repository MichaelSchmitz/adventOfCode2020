from typing import List
from copy import deepcopy

from .AbstractDay import AbstractDay


class CardDeck:
    def __init__(self, deck: List[str]) -> None:
        self.__deck = []
        for card in deck:
            self.__deck.append(int(card))

    def draw_top(self) -> int:
        return self.__deck.pop(0)

    def add(self, card: int) -> None:
        self.__deck.append(card)

    def card_count(self) -> int:
        return len(self.__deck)

    def get_deck_str(self) -> str:
        return ''.join(str(i) for i in self.__deck)

    def drop_till(self, limit: int) -> None:
        self.__deck = self.__deck[:limit]

    def winning_score(self) -> int:
        score = 0
        for i, card in enumerate(reversed(self.__deck)):
            score = score + card * (i + 1)
        return score


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]) -> None:
        super().__init__(puzzle_input)
        players = ''.join(puzzle_input).split('\n\n')
        self.player1 = CardDeck(players[0].split('\n')[1:])
        self.player2 = CardDeck(players[1].split('\n')[1:])
        self.player1_rec = deepcopy(self.player1)
        self.player2_rec = deepcopy(self.player2)

    def play(self):
        while self.player1.card_count() > 0 and self.player2.card_count() > 0:
            card1 = self.player1.draw_top()
            card2 = self.player2.draw_top()
            if card1 > card2:
                self.player1.add(card1)
                self.player1.add(card2)
            else:
                self.player2.add(card2)
                self.player2.add(card1)

    def play_recursive(self, card_deck1: CardDeck = None, card_deck2: CardDeck = None) -> bool:
        if card_deck1 is None:
            card_deck1 = self.player1_rec
        if card_deck2 is None:
            card_deck2 = self.player2_rec
        player1_won = False
        played_rounds = []
        while card_deck1.card_count() > 0 and card_deck2.card_count() > 0:
            this_config = card_deck1.get_deck_str() + ',' + card_deck2.get_deck_str()
            if this_config in played_rounds:
                return True
            else:
                played_rounds.append(this_config)
            card1 = card_deck1.draw_top()
            card2 = card_deck2.draw_top()
            player1_won = False
            if card_deck1.card_count() >= card1 and card_deck2.card_count() >= card2:
                sub_deck1 = deepcopy(card_deck1)
                sub_deck1.drop_till(card1)
                sub_deck2 = deepcopy(card_deck2)
                sub_deck2.drop_till(card2)
                if self.play_recursive(sub_deck1, sub_deck2):
                    player1_won = True
            else:
                if card1 > card2:
                    player1_won = True
            if player1_won:
                card_deck1.add(card1)
                card_deck1.add(card2)
            else:
                card_deck2.add(card2)
                card_deck2.add(card1)
        return player1_won

    def process_input_part1(self) -> int:
        self.play()
        if self.player1.card_count() == 0:
            return self.player2.winning_score()
        else:
            return self.player1.winning_score()

    def process_input_part2(self) -> int:
        self.play_recursive()
        if self.player1_rec.card_count() == 0:
            return self.player2_rec.winning_score()
        else:
            return self.player1_rec.winning_score()
