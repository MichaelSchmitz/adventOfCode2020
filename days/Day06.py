from typing import List

from .AbstractDay import AbstractDay


class CustomsDeclarationFormGroup:
    def __init__(self, group: str):
        self.ticked_per_person = []
        for person in group.split('\n'):
            person_set = set()
            for char in person:
                person_set.add(char)
            self.ticked_per_person.append(person_set)

    def get_count(self) -> int:
        return len(set.union(*self.ticked_per_person))

    def get_count_all_checked(self) -> int:
        return len(set.intersection(*self.ticked_per_person))


class Day(AbstractDay):
    def __init__(self, puzzle_input: List[str]):
        super().__init__(puzzle_input)
        self.groups = []
        puzzle = ''.join(self.puzzle_input).split('\n\n')
        for group in puzzle:
            self.groups.append(CustomsDeclarationFormGroup(group))

    def process_input_part1(self) -> int:
        return sum(group.get_count() for group in self.groups)

    def process_input_part2(self) -> int:
        return sum(group.get_count_all_checked() for group in self.groups)
