import common
from days import DayFactory
from time import perf_counter_ns


def process_day(day: str):
    start = perf_counter_ns()
    puzzle_input = common.read_input(day)
    day_processor = DayFactory.get_day(day, puzzle_input)
    common.print_day_result(day, 1, day_processor.process_input_part1())
    end = perf_counter_ns()
    print(f'Took {(end - start)/1000000}ms')
    start = perf_counter_ns()
    common.print_day_result(day, 2, day_processor.process_input_part2())
    end = perf_counter_ns()
    print(f'Took {(end - start)/1000000}ms')


if __name__ == '__main__':
    process_day('01')
    process_day('02')
    process_day('03')
    process_day('04')
    process_day('05')
    process_day('06')
    process_day('07')
    process_day('08')
    process_day('09')
    process_day('10')
    process_day('11')
    process_day('12')
    process_day('13')
    process_day('14')
    process_day('15')
    process_day('16')
    process_day('17')
    process_day('18')
    process_day('19')
    process_day('20')
    process_day('21')
    process_day('22')
    process_day('23')
    process_day('24')
    process_day('25')
